import Track from './src/track'

Track.install = function (Vue) {
  Vue.component(Track.name, Track)
}
export default Track
